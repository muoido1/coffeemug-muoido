# Lesson 1: Themes - Design Config

https://docs.google.com/presentation/d/1VOWH_jGFleyIIHxy0AyFhH6pRZyEXbIy5qP_m3_y9fk/edit#slide=id.gcb9a0b074_1_0
https://drive.google.com/file/d/14OKnEPfxFqxDHZd_izPM60qnB381ZJ76/view
https://www.youtube.com/playlist?list=PLRAec6rAAjcs4YNlY8HFJxsKCD-JASaOD

git config --global user.email "muoido@coffeemugvn.com"
git config --global user.name "Muoi Do"
### Có 2 cách cài theme

1. cài trực tiếp trong app/design/frontend app/design/adminhtml
2. cài qua composer thường code để trong vendor/

Default themes.

1. Blank frontend vendor/magento/theme-frontend-blank
2. Luma\* frontend vendor/magento/theme-frontend-luma
3. Backend adminhtml vendor/magento/theme-adminhtml-backend

Để override theme thì đặt tệp same location vào thư mục con

How are module specific files overridden?
● By placing the relevant file in the specified location within the module subdirectory of the theme
Module location (relative)
● view/<area>/path/to/file.type
Theme location (relative)
● <Vendor_Module>/path/to/file.type

By Default magento 2 with sample data comes with luma themes.

https://magento.stackexchange.com/questions/148422/magento-2-sample-data-and-logo-missing-in-custom-theme


Mỗi một widget id sẽ assign cho 1 theme
UPDATE `layout_link` SET `theme_id` = '4'
UPDATE `widget_instance` SET `theme_id` = '4'

Fix Required parameter 'theme_dir' was not passed : https://www.youtube.com/watch?v=-ns1GkytaZ0&ab_channel=TechnicalRajni


cart_page_product_thumbnail khi thay đổi hình ở trang giỏ hàng sẽ thay đổi theo

## 2. Layout XML / Templates
https://magemastery.net/courses/magento-2-beginner/module-in-magento-2

Làm thế nào các phần tử có thể được đặt đầu tiên/cuối cùng trong một phần tử cha?
● Sử dụng before="-" / after="-"

Làm cách nào để ghi đè các tệp mẫu trong một chủ đề?
● Bằng cách đặt tệp có liên quan ở cùng một vị trí trong child theme
● Khớp vị trí mẫu trong mô-đun sau khi  view/<area>
trong chủ đề dưới thư mục <Vendor_Module> 

How can you determine which template is
output on the frontend?
bin/magento dev:template-hints:enable
https://devdocs.magento.com/guides/v2.2/frontend-dev-guide/layouts/layout-override.html

● Add a container to the footer on all pages
● Add a block to the container that outputs a message
● Move the message on category pages to the header
https://inchoo.net/magento/custom-reference-structural-block/

● Add a child block to one of your previously created blocks
and output further content from another template
● Use arguments to pass and output data to the new child
block
● Escape the output in your templates
Override a layout file:
○ From a theme
○ From a module
How can you add a block to multiple pages
(but not all) without duplicating the layout
instructions?    

Use the bin/magento dev:template-hints:enable command to enable template hints and the bin/magento dev:template-hints:disable command to disable them. Do not forget to clear the cache after running the commands. For example:
