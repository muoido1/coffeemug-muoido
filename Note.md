## Setup a Magento 2 with Sample Data

![alt text](https://i.imgur.com/eGs0vro.png)

## Setup a Vietnamese Store (Vietnamese Language)

Stores > All Store > Create Store View > Điền thông tin

https://github.com/mageplaza/magento-2-vietnamese-language-pack

<!--- composer require mageplaza/magento-2-vietnamese-language-pack:dev-master mageplaza/module-smtp -->

bin/cli composer require mageplaza/magento-2-vietnamese-language-pack:dev-master

bin/magento setup:static-content:deploy vi_VN
bin/magento indexer:reindex
bin/magento cache:clean
bin/magento cache:flush

Stores > Configuration (Chọn store view bên góc trái) > General > Locale Options

System > Cache Management > Flush Magento Cache

Store việt nam đã có tiếng việt

![alt text](https://i.imgur.com/zJVTwJg.png)

## Add new various product types (2 -3 products for each type): Simple product, Configurable product, Grouped product, Virtual product, Bundle product, Downloadable product

Catalog -> Products

![alt text](https://i.imgur.com/23jtN2N.png)

## Add an open-source shipping method (Github) with sandbox testing

<!--- https://www.magepal.com/help/docs/configure-custom-shipping-methods-magento-2/#configuration
https://github.com/magepal/magento2-custom-shipping-rate

git clone https://github.com/magepal/magento2-custom-shipping-rate.git ./app/code/MagePal/CustomShippingRate

bin/magento module:enable --clear-static-content MagePal_CustomShippingRate

bin/magento setup:upgrade

git clone https://github.com/magepal/magento2-core.git ./app/code/MagePal/MagePalCore
bin/magento module:enable --clear-static-content MagePal_Core
bin/magento setup:upgrade

Log into your Magento 2 Admin

    Goto Stores
    Configuration
    MagePal
    Custom Shipping Rate -->

composer require magepal/magento2-customshippingrate
php -f bin/magento module:enable --clear-static-content ProgrammingAtKstark_CatewithimgWidget
php -f bin/magento setup:upgrade

## Add an open-source payment method (Github) with sandbox testing

<!-- https://github.com/Inchoo/magento2-Inchoo_Stripe
composer config repositories.inchoostripe git https://github.com/Inchoo/magento2-Inchoo_Stripe.git
composer require inchoo/stripe:dev-master
php bin/magento module:enable Inchoo_Stripe --clear-static-content
php bin/magento setup:upgrade

Stores/Configuration/ Sales/ Payment Methods/Stripe -->

composer require stripe/stripe-payments
php bin/magento setup:upgrade
php bin/magento cache:flush
php bin/magento cache:clean

Đặt hàng và thanh thoán thành công qua stripe xem đơn hàng tại sales > orders

![alt text](https://i.imgur.com/Xxtkq8L.png)

## Allow Vietnamese address books

chưa hiểu yêu cầu

https://developer.adobe.com/commerce/php/tutorials/frontend/custom-checkout/add-address-renderer/

https://github.com/EaDesgin/Magento2-City-Dropdown

bin/composer require eadesignro/romcity

bin/magento module:enable --clear-static-content Eadesigndev_RomCity

bin/magento setup:upgrade

bin/magento cache:clean
bin/magento cache:flush

## Allow credit memo created

Vào sales > order chọn đơn hàng > chọn invoice > Submit invoice
![alt text](https://i.imgur.com/9ZjSbym.png)

Trong đơn hàng chọn credit memo -> Update thông tin đơn hàng và số tiền cần hoàn lại và refund
![alt text] (https://i.imgur.com/COIdm1U.png)

## Understand and customize Order Status Workflow: https://www.mageplaza.com/kb/order-status-workflow-magento-2.html

Store > order status > tạo order status mới
Assign Status to State > gan status mới cho order state
Tại đơn hàng chọn status mới cho đơn hàng

## các loại product

### Grouped product - Sản phẩm nhóm

-   Combo điện thoại samsung : điện thoại, sạc dự phòng, ốp lưng, tai nghe ko dây chọn 1 trong số các món cũng được

### Configurable product - Sản phẩm tùy biến

-   Áo có size, màu , cân nặng
-   Xe máy chọn niềng , màu

### Bundle product - set sản phẩm tùy chọn

-   Combo sản phẩm Máy bộ pc: bên tron có ram, cpu , ổ cứng, đủ các bộ phận với thêm vào giỏ hàng được hoặc combo quà trong đó có nhiều loại custom chọn loại trà loại rượu

## Tax

Stores > Tax Zones and Rates > Add New Tax Rate > Điền thông tin ( Additional Settings thêm tax class)
Stores > Configuration > Sales > Tax

Cùng ưu tiên thì thuế cộng lại vd tax 1: 10% , tax 2: 5% --> tax = giá x (tax 1 + tax 2) = 100 x 15% = 15$
Khác ưu tiên thì thuế cộng lại vd tax 1: 10% (tax 1 order cao hơn nhân trước), tax 2: 5% --> tax = (giá x tax 1 ) + (giá + tax1) x tax 2 = 10$ + 110 x 5% = 15.5 $

(Chưa thấy set thuế theo category sản phẩm)

## promotion

Marketing > Promotions > Cart Price Rule.

Percent of product price discount: Giảm giá sản phẩm theo phần trăm
Fixed amount discount: Giảm giá số lượng tiền cố định. trên 1 sản phẩm
Fixed amount discount for whole cart: Giảm giá cố định cho toàn bộ giỏ hàng.
Buy X get Y free (discount amount is Y): Mua sản phẩm X tặng sản phẩm Y miễn phí.

Apply to Shipping Amount: Chọn có nếu bạn muốn áp dụng giảm giá cho cả tiền ship.
Discard subsequent rules: Chọn có nếu bạn không cho phép khách hàng áp dụng mã giảm giá khác sau khi đã áp dụng mã này.

bin/magento queue:consumers:start codegeneratorProcessor

Marketing > Promotions > Catalog Price Rule (Giảm giá trực tiếp trên sản phẩm)

custommer group

## Custom home -> tạo page

Category > add sub category > (add content and block)

Content > blog > add html (add widget)

Stores >> Configuration >> General >> Web >> Default Pages >> As u select CMS

Content > blog > Home Page Block - edit home page (Đổi home page block tại Content > Widget > Home page > Widget options )

## Shipping theo tỉnh, khối lượng

Flat rate: Phí theo % hoặc cố định, tính cho toàn đơn hoặc sản phẩm

Table rate:
Scope > main website >
https://www.tableratesgenerator.com/

## Customers > Customer Groups.

Stores → Configuration → Customers → Customer Configuration

## Maketking > Email template

# Custom theme

https://docs.google.com/presentation/d/1VOWH_jGFleyIIHxy0AyFhH6pRZyEXbIy5qP_m3_y9fk/edit#slide=id.gcb9a0b074_1_0
https://drive.google.com/file/d/14OKnEPfxFqxDHZd_izPM60qnB381ZJ76/view
https://www.youtube.com/playlist?list=PLRAec6rAAjcs4YNlY8HFJxsKCD-JASaOD

## NOTE

https://docs.google.com/spreadsheets/d/1zEKHnwhgAukwdt85ygoRs1EbdSX5nEdIJ92pQH3Fv5o/edit#gid=0

1. rm -rf var/cache var/page_cache var/view_preprocessed generated/code generated/metadata pub/static/frontend pub/static/adminhtml
2. php bin/magento cache:flush
3. php bin/magento setup:static-content:deploy -f

https://www.classcentral.com/course/youtube-magento-2-theme-development-from-zero-65188/classroom/6206a2440ca29
