## Lesson 1: Themes - Design Config

1. Create your own frontend theme in app/design
   ![](https://i.imgur.com/tICmuON.png)

2. Make some modifications & check they display
   Sửa size hình ở trang cart sang 150px

    ```bash

    <image id="cart_page_product_thumbnail" type="small_image">
        <width>150</width>
        <height>150</height>
    </image>

    ```

    hình sản phẩm được render lại size trên FE
    ![](https://i.imgur.com/ZONmvEv.png)

3. Đã assign custom theme cho từng store view
4. Mỗi widget được assign cho 1 theme nên khi đổi theme home page bị mất
   UPDATE `layout_link` SET `theme_id` = '4'
   UPDATE `widget_instance` SET `theme_id` = '4'

## Lesson 2: Layout XML / Templates

1.  Add a container to the footer on all pages
    ![](https://i.imgur.com/P3scX7e.png)

2.  Add a block to the container that outputs a message
    ![](https://i.imgur.com/bddtf3r.png)

3.  Move the message on category pages to the header

    src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/layout/catalog_category_view.xml

4.  Add a child block to one of your previously created blocks and output further content from another template

    Add code to template
    <?= $block->getChildHtml('coffemug.block.new') ?> // Nếu ko để tên thì có thể add block tuỳ thích nếu có tên thì chỉ block đúng tên với render ra

    add to defalt xml

    <referenceBlock name="coffeemug.first.layout.example">
        <block class="Magento\Framework\View\Element\Template" name="coffemug.block.new" template="Coffeemug_FirstPage::newblock.phtml" />
    </referenceBlock>

    ![]()

5.  Use arguments to pass and output data to the new child block

    ```bash

    add in xml

    <referenceBlock name="coffeemug.first.layout.example">
        <block class="Magento\Framework\View\Element\Template" name="coffemug.block.new" template="Coffeemug_FirstPage::newblock.phtml" >
            <arguments>
                <argument name="label" xsi:type="string">Block Label add by argument</argument>
                <argument name="argumentArray" xsi:type="array">
                    <item name="item1" xsi:type="string">Item 1</item>
                    <item name="item2" xsi:type="string">Item 2</item>
                    <item name="item3" xsi:type="string">Item 3</item>
                </argument>
            </arguments>
        </block>
            <block class="Magento\Framework\View\Element\Template" name="coffemug.block.new2" template="Coffeemug_FirstPage::content.phtml" />
    </referenceBlock>

    add in template

    <?=  $block->getdata('label')  ?>

    <ul>
    <?php
        $argumentArray = $block->getdata('argumentArray');
        foreach ($argumentArray as $key => $value) { ?>
            <li><?= $value ?></li>
    <?php } ?>
    </ul>

    ```

6.  Escape the output in your templates

    src/app/code/Coffeemug/FirstPage/view/frontend/templates/content.phtml

7.  Override a layout file:

    1. From a theme
       app/design/frontend/Coffeemug/muoido/Magento_Catalog/layout/catalog_product_view.xml

    ```bash
    <page layout="1column" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
        <body>
            <referenceBlock name="page.main.title">
                <block class="Magento\Framework\View\Element\Template" name="coffemug.block.new" template="Coffeemug_FirstPage::short_description_product.phtml" >
                </block>
            </referenceBlock>
        </body>
    </page>

    ```

    ![](https://i.imgur.com/8WOxBPF.png)

    2. From a module (Giữ nguyên file sửa gì thay đổi ở đó)

    src/app/design/frontend/Coffeemug/muoido/Magento_Checkout/layout/override/base/checkout_cart_index.xml
    src/app/design/frontend/Coffeemug/muoido/Magento_Checkout/templates/onepage/link.phtml

    ![](https://i.imgur.com/EzMzPfY.png)

8.  How can you add a block to multiple pages (but not all) without duplicating the layout instructions?

    /app/design/frontend/Coffeemug/muoido/Magento_Theme/page_layout/coffeemug-layout.xml

    Có thể sử dụng layout mới ở backend cho page hoặc sử dụng layout="coffeemug-layout" trong xml

    Test https://muoido.cmmage.app/about-us

#### note

    + Xem argument obj
    + Cách function lấy data trong controller để lấy ra xài trong template
    + Nếu override module để mở rộng chức năng thì các chức năng mới được sẽ được đặt ở thư mục nào (Cấu trúc thư mục controller, model)

## Lesson 3. Page Layouts / LESS / Deploy

1. Add a new page layout for product pages that adds an additional container
   src/app/design/frontend/Coffeemug/muoido/Magento_Theme/page_layout/coffeemug-layout.xml <!--  Nếu custom container ko có đata thì sẽ ko show ở FE -->
   src/app/design/frontend/Coffeemug/muoido/Magento_Theme/layouts.xml
   src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/layout/catalog_product_view.xml

2. Add another block to your new page layout container
   src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/layout/catalog_product_view.xml
   https://muoido.cmmage.app/beaumont-summit-kit.html
   Block sau Related Products

3. Remove the message on configurable product pages only
   src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/layout/catalog_product_view_type_configurable.xml
   https://muoido.cmmage.app/beaumont-summit-kit.html
   Add to cart đã mất thông báo

4. Modify gallery settings in etc/view.xml and review impact on product page
   src/app/design/frontend/Coffeemug/muoido/etc/view.xml
   https://muoido.cmmage.app/beaumont-summit-kit.html
   Xoá thumb và nav

5. Add a new variable to etc/view.xml and output in a template file
   src/app/design/frontend/Coffeemug/muoido/etc/view.xml
   src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/templates/new_section_product_page.phtml
   Block sau Related Products lấy variable từ view ra

6. Add a new CSS file via layout XML
   src/app/design/frontend/Coffeemug/muoido/Magento_Theme/layout/default_head_blocks.xml
   Add thêm file custom.css

7. Create a LESS file that matches the file added in layout
   src/app/design/frontend/Coffeemug/muoido/web/css/\_custom.less
   Đã tạo thêm file less

8. Change body colour using a variable (and a mixin)
   src/app/design/frontend/Coffeemug/muoido/web/css/custom.less
   Đã dổi bg sang #f8f8f8

9. Implement nesting and calculations to change the colour at a different breakpoint (screen size)
   src/app/design/frontend/Coffeemug/muoido/web/css/custom.less
   Màn hình dưới 921px bg trắng

10. Override a modules \_module.less and modify the styles
    ○ Try removing bulk of styles to see how bad the page looks
    src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/css/source/\_module.less
    https://muoido.cmmage.app/men/tops-men/jackets-men.html
    Tăng size title lên 25px

11. Revert above and use \_extend.less to make minor changes
    src/app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/css/source/\_extend.less
    https://muoido.cmmage.app/men/tops-men/jackets-men.html
    Tăng font giá và đổi sang màu đỏ

12. Read up on Magento LESS UI library
    https://developer.adobe.com/commerce/frontend-core/guide/css/ui-library/
13. Override variables using \_theme.less
    src/app/design/frontend/Coffeemug/muoido/web/css/source/\_theme.less
    https://muoido.cmmage.app/women/tops-women/jackets-women.html
    Tăng font rating và remove bg select

14. Override a default component from the library and modify
    src/app/design/frontend/Coffeemug/muoido/web/css/source/lib/\_navigation.less
    src/app/design/frontend/Coffeemug/muoido/web/css/source/lib/\_buttons.less
    Đổi bg menu và button

## Lesson 4. Javascript - RequireJS / jQuery

1. Add JS files to the page via <head/> in layout XML
   src/app/design/frontend/Coffeemug/muoido/web/js/muoido-custom.js
   src/app/design/frontend/Coffeemug/muoido/Magento_Theme/layout/default_head_blocks.xml

2. Create a RequireJS config file and add global files using dep (AMD module) and shim (non AMD module, with dependency)

    app/design/frontend/Coffeemug/muoido/web/requirejs-config.js.

3. Add a template via layout and use data-mage-init / x-magento-init to include a simple JS file that logs or alerts data

    app/design/frontend/Coffeemug/muoido/web/js/simple.js
    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/script-example.phtml

4. Add an alias in a RequireJS config file and update the file call in the template

    /www/muoido/files/src/app/design/frontend/Coffeemug/muoido/web/js/alias.js
    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/script-example.phtml

5. Pass config data to your JS file via the template, then by layout using jsLayout

    app/design/frontend/Coffeemug/muoido/web/js/config-data.js
    app/design/frontend/Coffeemug/muoido/Magento_Theme/layout/default.xml

6. Create a jQuery widget to perform a simple interaction on the page

    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/script-example.phtml
    app/design/frontend/Coffeemug/muoido/web/js/validation.js

7. Set default options and use data-role attributes to set default properties / roles to elements

    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/script-example.phtml

8. Create a mixin for your widget to modify it’s functionality

    app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/js/catalog-add-to-cart-mixins.js
    app/design/frontend/Coffeemug/muoido/Magento_Catalog/requirejs-config.js

# Lesson 5: Knockout / UI Components

1. Create a simple UI component (in a template) and render some text to an element using KO binding
   app/design/frontend/Coffeemug/muoido/web/js/knockout-js.js
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml

2. Override the text using config
   app/design/frontend/Coffeemug/muoido/web/js/knockout-js.js

3. Make the element observable and update after an interval

    app/design/frontend/Coffeemug/muoido/web/js/knockout-timer.js

4. Move the component JSON to layout XML (including config)
   app/design/frontend/Coffeemug/muoido/Magento_Theme/layout/default.xml

5. Render the element in a dedicated .html template file
   app/design/frontend/Coffeemug/muoido/Magento_Theme/web/template/timer.html

6. Add an observable array and loop through the elements
   app/design/frontend/Coffeemug/muoido/web/js/knockout-js.js
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml

7. Use ko.computed to manage multiple observables and update a child observable after an interval

    app/design/frontend/Coffeemug/muoido/web/js/knockout-js.js
    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml

8. Use the ‘click’ binding to update one of your created observables

    app/design/frontend/Coffeemug/muoido/web/js/knockout-js.js
    app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml

9. Create a secondary component and test out linking:
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml
   app/design/frontend/Coffeemug/muoido/web/js/parent-component.js
   app/design/frontend/Coffeemug/muoido/web/js/child-component.js

## Lesson 6. Translations / Customisations

1. Add translatable phrases in your theme for layout, .phtml and .html files
   app/design/frontend/Coffeemug/muoido/Magento_Theme/layout/default.xml
   app/design/frontend/Coffeemug/muoido/Magento_Theme/web/template/timer.html
   app/design/frontend/Coffeemug/muoido/Magento_Theme/web/template/person.html
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/custom_mainContainer.phtml
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/knockout-js.phtml
   app/design/frontend/Coffeemug/muoido/web/js/child-component.js

2. Export translation phrases using collect-phrases command
   bin/magento i18n:collect-phrases -o ./app/design/frontend/Coffeemug/muoido/i18n/theme-phrases.csv app/design/frontend/Coffeemug/muoido

3. Make some edits to the translations
   app/design/frontend/Coffeemug/muoido/i18n/vi_VN.csv

4. Create a language pack from the output from above
   app/design/frontend/Coffeemug/muoido/i18n/vi_VN.csv
5. Change locale in admin to the new locale and check the frontend

6. Review the list of files for generic page customisations in the swift otter study guide (section 8.1)
   ///ko hiểu

7. Make customisations (or override in your theme) to understand the impact the files have on the global frontend elements
   app/design/frontend/Coffeemug/muoido/i18n/vi_VN.csv

## Checkpoint 2

### Javascripts - RequireJS - JQuery

1. Đề bài 1: Add thư viện Slick Slider (Thư viện có sẵn )
   SS: https://imgur.com/a/1xdNG8M
   Add thư viện: app/design/frontend/Coffeemug/muoido/Magento_Catalog/requirejs-config.js
   Thư viện css: app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/css/source/\_slick.less
   Sửa cấu trúc html: app/design/frontend/Coffeemug/muoido/Magento_Catalog/templates/product/list/items.phtml
   Sửa icon add to wishlist: app/design/frontend/Coffeemug/muoido/Magento_Wishlist/templates/catalog/product/list/addto/wishlist.phtml
   CSS: app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/css/source/\_featured-product.less
   Sửa size hình: app/design/frontend/Coffeemug/muoido/etc/view.xml
2. Đề bài 2: Xử lý newsletter popup
   SS: https://imgur.com/VdDkNPN
   app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/subscribe.phtml
   app/design/frontend/Coffeemug/muoido/web/css/subscribe.less

### Knockout JS

1. Đề bài 1: Custom validation cho telephone
   https://magento.stackexchange.com/questions/225164/add-custom-validator-on-phonenumber-in-checkout
   https://magento.stackexchange.com/questions/209042/add-rule-to-rules-js-magento2/213630?stw=2#213630
   https://magento.stackexchange.com/questions/225164/add-custom-validator-on-phonenumber-in-checkout

    SS: https://imgur.com/iytVFTj
    app/design/frontend/Coffeemug/muoido/Magento_Checkout/layout/override/base/checkout_index_index.xml. line 374-401
    app/design/frontend/Coffeemug/muoido/requirejs-config.js
    app/design/frontend/Coffeemug/muoido/web/js/validation-mixin.js add thêm rule cho validation
    app/design/frontend/Coffeemug/muoido/web/js/validation-phone.js Check đổi rule validate khi select country

2. Đề bài 2: Tạo block mới cho để estimated delivery

    SS: https://imgur.com/CNZuw8R
    app/design/frontend/Coffeemug/muoido/Magento_Checkout/layout/override/base/checkout_index_index.xml line 736-749
    app/design/frontend/Coffeemug/muoido/Magento_Checkout/web/template/html-shipping-est.html
    app/design/frontend/Coffeemug/muoido/Magento_Checkout/web/js/view/shipping-est.js

### Translation / Customisations

1. Đề bài 1: Translations
   SS: https://imgur.com/Rq1tA2U và https://imgur.com/YoIGvC9
   app/design/frontend/Coffeemug/muoido/i18n/vi_VN.csv
2. Đề bài 2: Customize email template
   SS: https://imgur.com/TGW1RHQ
   app/design/frontend/Coffeemug/muoido/Magento_Email/email/header.html
   app/design/frontend/Coffeemug/muoido/Magento_Email/email/footer.html
   app/design/frontend/Coffeemug/muoido/Magento_Sales/email/order_new.html
   app/design/frontend/Coffeemug/muoido/Magento_Sales/web/css/source/\_email.less

https://stackoverflow.com/questions/74124011/how-to-send-data-from-checkout-to-magento2-controller

Bài 1: Trong directory app/code/Coffeemug tạo ra module ModuleExample

1. Tạo ra 1 controller GET mới, có thể truy cập từ website với url /coffeemugex1/page/page1 ( Page 1)
   SS: https://imgur.com/9EgMCzQ
   app/code/Coffeemug/ModuleExample/Controller/Page/Page1.php

2. Ở page trên get 1 block từ CMS với nội dung "Đây là văn bản lấy từ CMS block"
   SS: https://imgur.com/9EgMCzQ
   app/code/Coffeemug/ModuleExample/view/frontend/layout/coffeemugex1_page_page1.xml
   app/code/Coffeemug/ModuleExample/Controller/Page/Page1.php
   app/code/Coffeemug/ModuleExample/view/frontend/templates/content.phtml

3. Tạo ra 1 controller POST mới, với URL /coffeemugex1/expost/page1 (Post 1). Khi call đến URL này return 1 json là random 10 products trong hệ thống (Title, URL, Image, Price, Sale Price).
   SS: https://imgur.com/qQHe5nj
   app/code/Coffeemug/ModuleExample/Controller/Expost/page1.php

4. Ở Page 1, tạo 1 UI Component call đến Post 1, và hiển thị danh sách product ra ở dạng grid
   SS: https://imgur.com/D9u6IAv
   app/code/Coffeemug/ModuleExample/view/frontend/web/js/knockout-renderProduct.js
   app/code/Coffeemug/ModuleExample/view/frontend/web/template/productlist.html

Bài 2:

1. Tạo thêm attribute mới ( bằng cách lập trình ) với type là bool, Hiển thị field này lên category page, và product detail page bằng text và logo giao hàng nhanh. Lable: Giao hàng nhanh Type: Bool
   https://www.mageworx.com/blog/how-to-add-custom-field-for-option-values-in-advanced-product-options
   SS: https://imgur.com/6DYUtAw
   app/code/Coffeemug/ModuleExample/view/frontend/layout/catalog_category_view.xml
   app/code/Coffeemug/ModuleExample/view/frontend/templates/list.phtml Line 199-206
   app/code/Coffeemug/ModuleExample/Setup/InstallData.php
