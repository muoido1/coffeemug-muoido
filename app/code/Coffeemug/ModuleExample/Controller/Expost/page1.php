<?php

namespace Coffeemug\ModuleExample\Controller\Expost;

use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;

class Page1 extends  \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_productCollectionFactory;
    protected $_productFactory;
    protected $_imageHelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Helper\Image $imageHelper,

        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_imageHelper = $imageHelper;

        return parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->setPageSize(10);

        $products = [];
        foreach ($collection as $product) {
            // $product->getCustomAttribute('giaohangnhanh');
            $productData = $this->_productFactory
                ->create()
                ->load($product->getId());

            $url = $this->_imageHelper
                ->init($productData, 'product_thumbnail_image')
                ->setImageFile($product->getImage())
                ->resize(300, 400)
                ->getUrl();

            $productObject = [
                'id' => $productData->getId(),
                'name' => $productData->getName(),
                'url' => $productData->getProductUrl(),
                'price' => $productData->getPrice(),
                'sale_price' => $productData->getFinalPrice(),
                'image' => $url,
                'giaohangnhanh' => $productData->getCustomAttribute('giaohangnhanh') ? $productData->getCustomAttribute('giaohangnhanh')->getValue() : null
            ];
            array_push($products, $productObject);
        }

        /** @var Json $jsonResult */
        $jsonResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $jsonResult->setData($products);
        return $jsonResult;
    }
}
