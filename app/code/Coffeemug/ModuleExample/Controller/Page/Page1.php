<?php

namespace Coffeemug\ModuleExample\Controller\Page;

use Magento\Framework\App\ObjectManager;
use \Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\Controller\ResultFactory;


class Page1 extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory

    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        // var_dump(__METHOD__);

        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        /** @var Template $block */
        $block = $page->getLayout()->getBlock('coffeemug.module.layout.example');
        $block->setData('content_from_block', $this->content());

        return $page;
    }
    public function content()
    {
        $objectManager = ObjectManager::getInstance();
        $block = $objectManager->create(\Magento\Cms\Model\Block::class);
        $storeId = 1; //it can be any active store id.
        $blockId = 'BlockModuleExample'; // it can be any available block id.
        $block->setStoreId($storeId)->load($blockId);
        $html = $objectManager->create(FilterProvider::class)->getBlockFilter()->setStoreId($storeId)->filter($block->getContent());

        // echo $html;
        return   $html;
    }
}
