<?php

namespace Coffeemug\ModuleExample\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /* Product Custom Title */
        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'giaohangnhanh');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'giaohangnhanh',
            [
                'type' => 'int',
                'label' => 'Giao hàng nhanh',
                'input' => 'boolean',
                'required' => false,
                'sort_order' => 10,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'Giao hàng nhanh',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                // 'type' => 'int',
                // 'backend' => '',
                // 'frontend' => '',
                // 'label' => 'Giao Hang Nhanh',
                // 'input' => 'boolean',
                'class' => '',
                'source' => '',
                // 'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                // 'required' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => ''
            ]
        );
        // /* Product Custom Select Options */
        // $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'custom_mode');
        // $eavSetup->addAttribute(
        //     \Magento\Catalog\Model\Product::ENTITY,
        //     'custom_mode',
        //     [
        //         'type' => 'varchar',
        //         'label' => 'Custom Select Option',
        //         'input' => 'select',
        //         'required' => false,
        //         'sort_order' => 20,
        //         'source' => \Coffeemug\ModuleExample\Model\Config\Source\CustomModeList::class,
        //         'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
        //         'group' => 'custom_content_hide',
        //     ]
        // );
        // /* Product Custom Multi Select Option */
        // $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'custom_cms_pages');
        // $eavSetup->addAttribute(
        //     \Magento\Catalog\Model\Product::ENTITY,
        //     'custom_cms_pages',
        //     [
        //         'type' => 'varchar',
        //         'label' => 'Custom CMS Pages',
        //         'input' => 'multiselect',
        //         'required' => false,
        //         'sort_order' => 30,
        //         'source' => \Coffeemug\ModuleExample\Model\Config\Source\CMSPageList::class,
        //         'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
        //         'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
        //         'group' => 'custom_content_hide',
        //     ]
        // );
    }
}
