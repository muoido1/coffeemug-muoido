define(["jquery", "uiElement", "ko"], function ($, Component, ko) {
    "use strict";
    return Component.extend({
        defaults: {
            listProduct: ko.observableArray([]),
            tracks: {
                listProduct: true,
            },
        },
        initObservable: function () {
            this._super().observe("listProduct");
            return this;
        },
        initialize: function (config) {
            console.log("knockout js load");
            $.ajax({
                url: config.url,
                type: "POST",
                dataType: "json",
                showLoader: true,
                success: function (data) {
                    this.listProduct(data);
                }.bind(this),
            });
            this._super();
            return this;
        },
    });
});
