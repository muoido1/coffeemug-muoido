/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    shim: {
        "Magento_Catalog/js/slick.min": ["jquery"],
    },

    config: {
        mixins: {
            "Magento_Catalog/js/catalog-add-to-cart": {
                "Magento_Catalog/js/catalog-add-to-cart-mixins": true,
            },
        },
    },
};
