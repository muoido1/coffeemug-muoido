/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    "Magento_Checkout/js/model/resource-url-manager",
    "Magento_Checkout/js/model/quote",
    "mage/storage",
    "Magento_Checkout/js/model/shipping-service",
    "Magento_Checkout/js/model/shipping-rate-registry",
    "Magento_Checkout/js/model/error-processor",
], function (
    resourceUrlManager,
    quote,
    storage,
    shippingService,
    rateRegistry,
    errorProcessor
) {
    "use strict";

    return {
        /**
         * @param {Object} address
         */
    };
});
