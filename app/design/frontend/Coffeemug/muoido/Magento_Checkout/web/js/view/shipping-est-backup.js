/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    "uiComponent",
    "Magento_Checkout/js/model/totals",
    "ko",
    "jquery",
    "Magento_Checkout/js/action/select-shipping-address",
    "Magento_Checkout/js/model/address-converter",
    "Magento_Checkout/js/model/cart/estimate-service",
    "Magento_Checkout/js/checkout-data",
    "Magento_Checkout/js/model/shipping-rates-validator",
    "uiRegistry",
    "Magento_Checkout/js/model/quote",
    "Magento_Checkout/js/model/checkout-data-resolver",
    "Magento_Checkout/js/model/shipping-service",
    "Magento_Ui/js/form/element/select",
    "Magento_Customer/js/customer-data",
    "domReady!",
], function (
    Component,
    totals,
    ko,
    $,
    selectShippingAddress,
    addressConverter,
    estimateService,
    checkoutData,
    shippingRatesValidator,
    registry,
    quote,
    checkoutDataResolver,
    shippingService,
    formSelect,
    customerData
) {
    "use strict";
    var countryData = customerData.get("directory-data");
    return Component.extend({
        isLoading: totals.isLoading,

        defaults: {
            country_id: ko.observable(""),
            imports: {
                countryOptions: "${ $.provider2 }.country_id:indexedOptions",
                update: "${ $.provider2 }.country_id:value",
            },
        },

        initialize: function () {
            this._super();
            // var address;
            // console.log(fName);
            shippingService.isLoading(false);
            console.log("load shipping success");
            console.log(countryData()["VN"]);
            // console.log(this.getCountryName("VN"));
            // console.log(this.test);
            // console.log(customerData.get("directory-data"));
            // console.log(country);
            // console.log(region);
            // console.log(quote);
            // console.log(quote.shippingAddress());
            // console.log(
            //     addressConverter.formAddressDataToQuoteAddress(
            //         checkoutData.getSelectedBillingAddress()
            //     )
            // );

            // address = quote.isVirtual()
            //     ? quote.billingAddress()
            //     : quote.shippingAddress();

            // if (!address && quote.isVirtual()) {
            //     address = addressConverter.formAddressDataToQuoteAddress(
            //         checkoutData.getSelectedBillingAddress()
            //     );
            // }
            // console.log(address);
            // this.country_id = ko.observable(address.countryId);
            // this.loadCountry();
            // $(".select").on("change", function () {
            //     this.country_id("timespan");
            // });
            // setInterval(this.loadCountry.bind(this), 1000);
            // this.loadCountry.bind(this);
            return this;
        },
        loadCountry: function () {
            var address = quote.isVirtual()
                ? quote.billingAddress()
                : quote.shippingAddress();

            if (!address && quote.isVirtual()) {
                address = addressConverter.formAddressDataToQuoteAddress(
                    checkoutData.getSelectedBillingAddress()
                );
            }
            // console.log(address);
            this.country_id(address.countryId);
            // console.log(country);
            // console.log(region);
            // console.log(customerData.get("cart"));

            // $(".select").on("change", function () {
            //     this.country_id("timespan");
            // });
        },
        countryChanged: function (value) {
            console.log(value);
        },
        update: function (params) {
            console.log(this.getCountryNameNew(params));
            // console.log(countryData()[params].name);
            var estTime =
                "The order will be delivered to " +
                this.getCountryNameNew(params) +
                " in next 5-7 days.";
            if (params == "VN") {
                estTime =
                    "The order will be delivered to " +
                    this.getCountryNameNew(params) +
                    " in next 2-4 days.";
            }
            if (params == "US") {
                estTime =
                    "The order will be delivered to " +
                    this.getCountryNameNew(params) +
                    " in next 11-14 days.";
            }
            console.log(estTime);
            return estTime;
        },
        countryOptions: function (params) {
            console.log(params);
        },
        getCountryNameNew: function (countryId) {
            return countryData()[countryId] != undefined
                ? countryData()[countryId].name
                : ""; //eslint-disable-line
        },
    });
});
