/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    "uiComponent",
    "Magento_Checkout/js/model/totals",
    "ko",
    "jquery",
    "Magento_Checkout/js/action/select-shipping-address",
    "Magento_Checkout/js/model/address-converter",
    "Magento_Checkout/js/model/cart/estimate-service",
    "Magento_Checkout/js/checkout-data",
    "Magento_Checkout/js/model/shipping-rates-validator",
    "uiRegistry",
    "Magento_Checkout/js/model/quote",
    "Magento_Checkout/js/model/checkout-data-resolver",
    "Magento_Checkout/js/model/shipping-service",
    "Magento_Ui/js/form/element/select",
    "Magento_Customer/js/customer-data",
    "domReady!",
], function (
    Component,
    totals,
    ko,
    $,
    selectShippingAddress,
    addressConverter,
    estimateService,
    checkoutData,
    shippingRatesValidator,
    registry,
    quote,
    checkoutDataResolver,
    shippingService,
    formSelect,
    customerData
) {
    "use strict";
    var countryData = customerData.get("directory-data");
    return Component.extend({
        isLoading: totals.isLoading,

        defaults: {
            shipping_html: ko.observable(""),
            imports: {
                countryOptions: "${ $.provider2 }.country_id:indexedOptions",
                update: "${ $.provider2 }.country_id:value",
                test: "checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.country_id:value",
            },
        },

        initialize: function () {
            this._super();

            shippingService.isLoading(false);

            return this;
        },

        countryChanged: function (value) {
            console.log(value);
        },
        update: function (params) {
            var html =
                "The order will be delivered to " +
                this.getCountryNameNew(params) +
                " in next 5-7 days.";
            if (params == "VN") {
                html =
                    "The order will be delivered to " +
                    this.getCountryNameNew(params) +
                    " in next 2-4 days.";
            }
            if (params == "US") {
                html =
                    "The order will be delivered to " +
                    this.getCountryNameNew(params) +
                    " in next 11-14 days.";
            }
            this.shipping_html(html);
            console.log(html);
            return html;
        },
        getCountry: function () {
            return this.shipping_html;
        },
        getCountryNameNew: function (countryId) {
            return countryData()[countryId] != undefined
                ? countryData()[countryId].name
                : ""; //eslint-disable-line
        },
    });
});
