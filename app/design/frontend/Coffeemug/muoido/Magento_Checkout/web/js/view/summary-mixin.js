/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    "uiComponent",
    "Magento_Checkout/js/model/totals",
    "ko",
    "jquery",
    "Magento_Checkout/js/action/select-shipping-address",
    "Magento_Checkout/js/model/address-converter",
    "Magento_Checkout/js/model/cart/estimate-service",
    "Magento_Checkout/js/checkout-data",
    "Magento_Checkout/js/model/shipping-rates-validator",
    "uiRegistry",
    "Magento_Checkout/js/model/quote",
    "Magento_Checkout/js/model/checkout-data-resolver",
    "Magento_Checkout/js/model/shipping-service",
    "mage/validation",
    "Magento_Ui/js/form/element/select",
], function (
    Component,
    totals,
    ko,
    $,
    selectShippingAddress,
    addressConverter,
    estimateService,
    checkoutData,
    shippingRatesValidator,
    registry,
    quote,
    checkoutDataResolver,
    shippingService,
    formSelect
) {
    "use strict";

    return Component.extend({
        isLoading: totals.isLoading,

        defaults: {
            country_id: ko.observable(""),

            // imports: {
            //     status: "${ $.provider }:status",
            // },
            tracks: {
                country_id: true,
            },
            links: {
                fName: "checkoutProvider:shippingAddress.firstname",
            },
            // links: {
            //     bio: "${ $.provider }:bio",
            // },
        },

        initialize: function () {
            this._super();
            var address;
            // console.log(fName);
            shippingService.isLoading(false);

            address = quote.isVirtual()
                ? quote.billingAddress()
                : quote.shippingAddress();

            if (!address && quote.isVirtual()) {
                address = addressConverter.formAddressDataToQuoteAddress(
                    checkoutData.getSelectedBillingAddress()
                );
            }
            // console.log(address);
            // this.country_id = ko.observable(address.countryId);
            // this.loadCountry();
            // $(".select").on("change", function () {
            //     this.country_id("timespan");
            // });
            // setInterval(this.loadCountry.bind(this), 1000);
            // this.loadCountry.bind(this);
            return this;
        },
        loadCountry: function () {
            var address = quote.isVirtual()
                ? quote.billingAddress()
                : quote.shippingAddress();

            if (!address && quote.isVirtual()) {
                address = addressConverter.formAddressDataToQuoteAddress(
                    checkoutData.getSelectedBillingAddress()
                );
            }
            // console.log(address);
            this.country_id(address.countryId);
            // $(".select").on("change", function () {
            //     this.country_id("timespan");
            // });
        },
        getCountry: function () {
            return this.country_id;
        },
    });
});
