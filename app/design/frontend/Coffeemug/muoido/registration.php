<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/Coffemug/muoido', __DIR__);
// \Magento\Framework\Component\ComponentRegistrar::register(
//     \Magento\Framework\Component\ComponentRegistrar::LANGUAGE,
//     'muoido_vi_VN',
//     __DIR__
// );
