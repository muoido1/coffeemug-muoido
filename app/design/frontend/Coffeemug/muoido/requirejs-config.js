var config = {
    deps: ["jquery"],

    map: {
        "*": {
            simple: "js/simple",
            aliasJS: "js/alias",
            configData: "js/config-data",
            validation: "js/validation",
        },
    },
    shim: {
        "/js/owl-carousel": ["jquery"],
    },
    config: {
        mixins: {
            // "Magento_Ui/js/lib/validation/validator": {
            //     "js/validation-mixin": true,
            // },
            "mage/validation": {
                "js/validation-mixin": true,
            },
        },
    },
};
