define(["jquery"], function () {
    "use strict";
    return function (config, element) {
        if (element) {
            console.log({
                element: element,
                config: config,
            });
        }
    };
});

// define(["uiComponent"], function (Component) {
//     "use strict";
//     return Component.extend({
//         initialize: function (config, node) {
//             console.log(config);
//             console.log(node);
//         },
//     });
// });
