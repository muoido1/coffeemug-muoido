define(["jquery"], function () {
    "use strict";
    return function (config, element) {
        if (element) {
            console.log({
                element: element,
                config: config,
            });
        }
    };
});
