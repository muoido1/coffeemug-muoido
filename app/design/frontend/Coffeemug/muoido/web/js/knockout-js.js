define(["jquery", "uiComponent", "ko"], function ($, Component, ko) {
    "use strict";
    return Component.extend({
        clock: ko.observable(""),
        initialize: function (config) {
            this.heading = ko.observable(config.timerMessage);
            this.fname = ko.observable("Do");
            this.lname = ko.observable("Muoi");
            this.count = ko.observable(0);

            this.name = ko.computed(function () {
                return (
                    this.fname() + " Van " + this.lname() + " " + this.clock()
                );
            }, this);

            this.getNumbers = ko.observableArray([2, 4, 6, 8, 10]);
            this.getCountryCurrency = ko.observableArray([
                {
                    country: "United State",
                    currency: "USD",
                    rate: 1.0,
                },
                {
                    country: "England",
                    currency: "EUR",
                    rate: 1.5,
                },
                {
                    country: "India",
                    currency: "INR",
                    rate: 2,
                },
            ]);
            setInterval(this.reloadTime.bind(this), 1000);
        },
        reloadTime: function () {
            var now = new Date().toTimeString().split(" ")[0];
            this.clock(now);
        },
        removeFullname: function () {
            this.fname("");
            this.lname("");
            this.clock("");
        },
    });
});
// define(["ko"], function (ko) {
//     "use strict";
//     return function (config) {
//         console.log(config);
//         this.heading = ko.observable(config.timerMessage);
//     };
//     // initialize: function () {
//     //     this.customerName = ko.observableArray([]);
//     //     this.customerData = ko.observable("");
//     //     this._super();
//     // },
//     // addNewCustomer: function () {
//     //     this.customerName.push({ name: this.customerData() });
//     //     this.customerData("");
//     // },
// });
