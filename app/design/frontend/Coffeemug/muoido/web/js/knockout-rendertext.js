define(["uiElement", "ko"], function (Component, ko) {
    "use strict";

    return Component.extend({
        defaults: {
            bio: ko.observable(),
        },

        initialize: function () {
            this._super();
            return this;
        },
      
    });
});
