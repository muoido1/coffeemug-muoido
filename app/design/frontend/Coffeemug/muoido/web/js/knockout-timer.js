define(["uiComponent", "ko", "js/plugins/countdown.min"], function (
    Component,
    ko
) {
    return Component.extend({
        clock: ko.observable(""),
        initialize: function () {
            this._super();
            setInterval(this.reloadTime.bind(this), 1000);
        },
        reloadTime: function () {
            var end = new Date("Sept 1 , 2023 15:24:24");
            var now = new Date();
            var timespan = countdown(now, end);
            this.clock(timespan);
        },
        getClock: function () {
            return this.clock;
        },
    });
});
