define(["uiComponent"], function (Component) {
    "use strict";

    return Component.extend({
        defaults: {
            superheroes: ["Batman", "Superman"],
            provider: "supervillains",
            supervillains: "",
            tracks: {
                superheroes: true,
                supervillains: true,
            },
            imports: {
                supervillains: "${$.provider}:supervillains",
            },
        },
    });
});
