define(["uiComponent"], function (Component) {
    "use strict";
    return Component.extend({
        defaults: {
            supervillains: ["Joker", "Lex Luther"],
            tracks: {
                supervillains: true,
            },
        },
    });
});
