// define(["jquery", "mage/translate"], function ($) {
//     "use strict";

//     return function (validator) {
//         validator.addRule(
//             "validate-phone",
//             function (value) {
//                 console.log(value);
//                 return (
//                     value.length == 12 &&
//                     value.match(/(((\+|)84))+([0-9]{9})\b/)
//                 );
//             },
//             $.mage.__(
//                 "The phone number should match with Viet Nam phone number (+84XXXXXXXXX)"
//             )
//         );

//         return validator;
//     };
// });
define(["jquery"], function ($) {
    "use strict";

    return function (targetWidget) {
        $.validator.addMethod(
            "validate-phone",
            function (value) {
                console.log(value);
                return (
                    value.length == 12 &&
                    value.match(/(((\+|)84))+([0-9]{9})\b/)
                );
            },
            $.mage.__(
                "The phone number should match with Viet Nam phone number (+84XXXXXXXXX)"
            )
        );
        return targetWidget;
    };
});
