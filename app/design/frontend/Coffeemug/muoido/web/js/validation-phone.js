define(["underscore", "Magento_Ui/js/form/element/abstract"], function (
    _,
    Abstract
) {
    "use strict";
    return Abstract.extend({
        defaults: {
            imports: {
                update: "${ $.parentName }.country_id:value",
            },
        },

        initialize: function () {
            console.log("component success");
            return this._super();
        },

        update: function (value) {
            console.log(this);
            if (value == "VN") {
                this["validation"]["validate-phone"] = true;
                this.value("+84");
                this["validation"]["max_text_length"] = 12;
            } else {
                this["validation"]["validate-phone"] = false;
                this.value("");
                this["validation"]["max_text_length"] = 30;
            }
        },
    });
});
