define(["jquery"], function ($, config, element) {
    "use strict";
    console.log(config, element);
    $.widget("mage.validation", {
        _create: function (config, element) {
            this._case = $(this.element);

            $(this._case).keyup(function () {
                var max = 150;
                var len = $(this.element).length;
                console.log(config, element);
                if (len >= max) {
                    $("#charNum").text(" you have reached the limit");
                } else {
                    var char = max - len;
                    $("#charNum").text(char + " characters left");
                }
            });
        },
    });

    return $.mage.validation;
});
