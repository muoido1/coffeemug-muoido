## Theme / Design config

src/app/design/frontend/Coffeemug/muoido/theme.xml
src/app/design/frontend/Coffeemug/muoido/registration.php

## Layout XML / Templates

### Đề bài 1: Custom layout CMS Page

https://muoido.cmmage.app/checkpoint-1-layout

app/design/frontend/Coffeemug/muoido/web/css/checkpoint1.less
src/app/design/frontend/Coffeemug/muoido/Magento_Theme/page_layout/checkpoint1-layout.xml
src/app/design/frontend/Coffeemug/muoido/Magento_Theme/templates/html/checkpoint1.phtml

### Đề bài 2: Image Zooming

app/design/frontend/Coffeemug/muoido/etc/view.xml
override app/design/frontend/Coffeemug/muoido/web/magnifier/magnifier.js
app/design/frontend/Coffeemug/muoido/web/magnifier/magnifier.js

### Đề bài 3: Show block grid category on homepage

https://muoido.cmmage.app/

app/code/ProgrammingAtKstark/CatewithimgWidget
src/app/design/frontend/Coffeemug/muoido/ProgrammingAtKstark_CatewithimgWidget/templates/widget/categorywidget.phtml
src/app/design/frontend/Coffeemug/muoido/ProgrammingAtKstark_CatewithimgWidget/web/css/widget.css

## Page layout / LESS / Deploy

### Đề bài 1: Add custom font

src/app/design/frontend/Coffeemug/muoido/web/css/source/\_typography.less
src/app/design/frontend/Coffeemug/muoido/web/css/source/\_variables.less

### Đề bài 2: Custom page layout trang danh sách sản phẩm

https://muoido.cmmage.app/gear/bags.html

app/design/frontend/Coffeemug/muoido/Magento_Catalog/web/css/source/\_custom.less
app/design/frontend/Coffeemug/muoido/Magento_PageBuilder/page_layout/catalog_category_view.xml
app/design/frontend/Coffeemug/muoido/Magento_Catalog/templates/product/list.phtml
app/design/frontend/Coffeemug/muoido/Magento_PageBuilder/templates/catalog/category/view/description.phtml

git remote set-url origin https://gitlab.com/muoido1/coffeemug-muoido.git